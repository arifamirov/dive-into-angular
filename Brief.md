# Dive into Angular

The project aimed to help us to dive into the world of front-end development

Dive into Angular - In 4 weeks

Goal: be ready to work on Waterguard tickets by the end of June.

Knowledge expected:
  - learn the basics of Angular development:
    - create new Angular components (for presentation and for controlling data)
    - create Angular services
    - make REST calls and process/display returned data
    - use Angular Material for presentation
    - have a project structure
    - demo your app
    - learn the basics of NgRx Store (state management)
    - learn the basics of functional reactive programming with RxJs
  - learn the dev tools: [IDE of your choice, Git, JIRA]

Principles of learning:
  - learning by doing
  - take responsibility for your learning process
  - not knowing something is a valid and acceptable state. not asking/telling about it is not so much
  - try to enjoy learning/solving your task

Process:
  - start the day with planning standups:
    - plan your day. where are you now, what are you going to do today
    - identify your problems. break it down to smaller, actionable tasks
    - ask for support if stuck
  - make at least one commit per day
  - demo your app for the team at the end of the week

Week 1-2:
  task: build a simple app in Angular displaying programming jokes
  features:
    - one main page at first
    - display the app name
    - display a list of 5 jokes
    - display a button to load another 5 jokes
      - display options to select category and filter
    - extra: display random jokes
  resources:
    - Bitbucket: create new repository in the cloud with your company email
    - public API of jokes: https://sv443.net/jokeapi
    - Angular Docs and CLI: https://angular.io/cli
    - Angular Material: https://material.angular.io/components/categories
    - Typescript: http://www.typescriptlang.org/docs/handbook/basic-types.html

Week 3-4:
  task: extend the app with NgRx Store
  features:
    - store the jokes in NgRx after REST call
    - display jokes coming from the store
    - extra: make the store permanent between page reloads
  resources:
    - NgRx: https://ngrx.io/docs
    - RxJs: https://rxjs.dev/guide/overview