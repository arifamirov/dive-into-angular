import { Component, OnInit} from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  url$: Observable<string>;
  constructor(private router: Router) {}

  ngOnInit(): void {
    this.url$ = this.router.events.pipe(
      filter(event => event instanceof NavigationStart),
      map((event: NavigationStart) => event.url),
    );
  }
}
