import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {JokeService} from './services/joke.service';

import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { ChildComponent } from './child/child.component';
import { MaterialModule } from './material.module';
import { SetupPageComponent } from './setup-page/setup-page.component';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';

const routes: Routes = [
  {path: 'setup', component: SetupPageComponent},
  {path: '', component: HomepageComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ChildComponent,
    SetupPageComponent,
    HomepageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    RouterModule.forRoot(routes),
    NgxQRCodeModule,
  ],
  providers: [JokeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
