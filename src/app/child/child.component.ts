import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Joke, ValidFlags } from '../joke.type';
import { stringify } from '@angular/core/src/util';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent {
  @Input() jokes: Joke[] = [];
  @Input() flags: ValidFlags[] = [];
  @Output() markJoke: EventEmitter<number> = new EventEmitter();
  @Output() markQR: EventEmitter<number> = new EventEmitter();

  elementType: 'url' | 'canvas' | 'img' = 'url';

  checkTheList(joke: Joke, flags: ValidFlags[]): boolean {
    for (const flag of this.flags) {
      if (joke[flag]) {
        return false;
      }
    }
    return true;
  }
}

