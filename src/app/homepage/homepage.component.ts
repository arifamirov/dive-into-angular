import { Component, OnInit} from '@angular/core';
import { JokeService } from '../services/joke.service';
import { Joke, ValidFlags } from '../joke.type';
import { MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { FavService } from '../services/fav.service';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'homepage-root',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  jokes: Joke[] = [];
  flags: ValidFlags[] = [];

  constructor(public joke: JokeService, public errorsM: MatSnackBar, private router: Router, public fav: FavService) {}

  ngOnInit() {
    this.jokes = this.fav.getAllJokes();
    this.flags = this.fav.getAllFlags();
    if (this.jokes.length === 0) {this.fav.getDataProg(0); }
  }

  scrollUp() {
    window.scrollTo(0, 0);
  }

  markFavourite(id: number) {
    this.fav.markFavourite(id);
  }

  markQR(id: number) {
    this.fav.markQR(id);
  }

  getDataMiscell(i: number): void {
    this.fav.getDataMiscell(i);
  }

  getDataProg(i: number): void {
    this.fav.getDataProg(i);
  }

  updateFlags(flag: ValidFlags): void {
    this.fav.updateFlags(flag);
  }

}
