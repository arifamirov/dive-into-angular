export interface Joke {
  category: string;
  type: string;
  joke?: string;
  setup?: string;
  delivery?: string;
  nsfw?: boolean;
  religious?: boolean;
  political?: boolean;
  id: number;
  favourite?: boolean;
  qrcode?: boolean;
}

export type ValidFlags = 'nsfw' | 'political' | 'religious';
