import { Injectable } from '@angular/core';
import { Joke, ValidFlags } from '../joke.type';
import { JokeService } from './joke.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { of } from 'rxjs';
import { element } from '@angular/core/src/render3';

@Injectable({
  providedIn: 'root'
})
export class FavService {
  private errorMsg: string[] = [];
  private jokes: Joke[] = [];
  private flags: ValidFlags[] = [];

  constructor(
    private jokeApi: JokeService,
    private errorsM: MatSnackBar,
  ) {}

  updateFlags(flag: ValidFlags): void {
    if (this.flags.includes(flag)) {
      const index: number = this.flags.indexOf(flag);
      this.flags.splice(index, 1);
    } else {
      this.flags.push(flag);
    }
    console.log('flags', this.flags);
  }

  getAllFav(): Joke[] {
// tslint:disable-next-line: prefer-for-of
    const fav: Joke[] = [];
    for (let i = 0; i < this.jokes.length; i++) {
      if (this.jokes[i].favourite) {
        fav.push(this.jokes[i]);
      }
    }
    return fav;
  }

  getAllJokes(): Joke[] {
    return this.jokes;
  }

  getAllFlags(): ValidFlags[] {
    return this.flags;
  }

  getDataMiscell(i: number): void {
    console.log(this.flags);
    for (i; i < 5; i++) {
      this.jokeApi.blackListF('Miscellaneous', this.flags).subscribe(data => {
        const exists: boolean = this.jokes.findIndex(joke => joke.id === data.id) >= 0;
        if (exists) {
          this.getDataMiscell(i - 1);
        } else {
          this.jokes.push(data);
        }
      },
        (error: HttpErrorResponse) => {
          this.errorHandler(error);
        });
    }
    this.clearErrors();
  }

   getDataProg(i: number): void {
    console.log(this.flags);
    for (i; i < 5; i++) {
      this.jokeApi.blackListF('Programming', this.flags).subscribe(data => {
        const exists: boolean = this.jokes.findIndex(joke => joke.id === data.id) >= 0;
        if (exists) {
          this.getDataProg(i - 1);
        } else {
          this.jokes.push(data);
        }
      }, (error: HttpErrorResponse) => {
        this.errorHandler(error);
      });
    }
    this.clearErrors();
  }

  private errorHandler(error: HttpErrorResponse): void {
    if (!this.errorExists(error)) {
      if (error.name === 'HttpErrorResponse') {
        // tslint:disable-next-line: max-line-length
        this.errorsM.open('Network error or too many requests. Check your connection or wait a minute before loading more jokes', 'Ok',
          {
            duration: 5000,
            panelClass: ['my-snack-bar']
          });
      } else { this.errorsM.open('We have unexpected error. Our team is working on it.', 'Ok', { duration: 5000 }); }
    }
  }

  private errorExists(error: HttpErrorResponse): boolean {
    if (this.errorMsg.length === 0) {
      this.errorMsg.push(error.message);
      return false;
    }

    for (const msg of this.errorMsg) {
      if (msg !== error.message) {
        this.errorMsg.push(error.message);
        return false;
      }
    }

    return true;
  }

  private clearErrors() {
    // tslint:disable-next-line: prefer-for-of
    for (let j = 0; j < this.errorMsg.length; j++) {
      this.errorMsg.pop();
    }
  }

  markFavourite(id: number) {
    const index: number = this.jokes.findIndex(joke => joke.id === id);
    this.jokes[index].favourite = !this.jokes[index].favourite;
  }

  markQR(id: number) {
    const index: number = this.jokes.findIndex(joke => joke.id === id);
    this.jokes[index].qrcode = !this.jokes[index].qrcode;
  }
}
