import { HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import { Joke, ValidFlags } from '../joke.type';
import { Injectable } from '@angular/core';
import {throwError, Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JokeService {
  apiBase = 'https://sv443.net/jokeapi/category/';

  constructor(public http: HttpClient) { }

  blackListF(categoryName = 'Any', flags: ValidFlags[] = []): Observable<Joke> {
    const options = {
      params: new HttpParams().set('blacklistFlags', flags.join(',')),
    };

    const path = ''.concat(this.apiBase, categoryName);

    return this.http.get<Joke>(path, options).pipe(catchError(err => {
      return throwError(err);
    }));
  }

}
