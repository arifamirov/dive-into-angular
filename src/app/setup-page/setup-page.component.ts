import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Joke, ValidFlags } from '../joke.type';
import { FavService } from '../services/fav.service';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'setup-page-root',
  templateUrl: './setup-page.component.html',
  styleUrls: ['./setup-page.component.css']
})
export class SetupPageComponent implements OnInit {
  @Input() jokes: Joke[] = [];
  @Input() flags: ValidFlags[] = [];

  favJokes: Joke[] = [];
  FavTitle = 'You have no favourite jokes yet';

  constructor(private router: Router, private fav: FavService) { }

  ngOnInit() {
    this.favJokes = this.fav.getAllFav();
    this.jokes = this.fav.getAllJokes();
  }

  markFavourite(id: number) {
    this.fav.markFavourite(id);
  }

  markQR(id: number) {
    this.fav.markQR(id);
  }

}
